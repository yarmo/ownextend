const express = require('express')
const app = express()
const path = require('path')

app.set('env', process.env.NODE_ENV || "production")
app.set('view engine', 'pug')
app.set('port', process.env.PORT || 3000)

app.use('/static', express.static(path.join(__dirname, 'static')))
app.use('/static', express.static(path.join(__dirname, 'libs')))

app.get('/', (req, res) => {
  res.render('index')
})
app.get('/chat', (req, res) => {
  res.render('chat', { 'title': 'Chat' })
})
app.get('/tts', (req, res) => {
  res.render('tts', { 'title': 'TTS' })
})
app.get('/countdown', (req, res) => {
  res.render('countdown', { 'title': 'Countdown' })
})
app.get('/dynmessage', (req, res) => {
  res.render('dynmessage', { 'title': 'Dynamic Message' })
})
app.get('/liberapay', (req, res) => {
  res.render('liberapay', { 'title': 'Liberapay' })
})

app.listen(app.get('port'), () => {
  console.log(`App listening at http://localhost:${app.get('port')}`)
})
