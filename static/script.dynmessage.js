const runDynmessage = (input) => {
  const match = input.match(/([^\/]*)\/([^\/]*)(?:\/([^\/]))?/)
  const message = decodeURIComponent(match[1])
  const domain = decodeURIComponent(match[2])
  const refreshRateSeconds = match[3] ? parseInt(decodeURIComponent(match[3])) : 0
  const toolEl = document.querySelector('#tool')

  let dynmessageEl = document.createElement('p')
  dynmessageEl.classList.add('dynmessage')
  toolEl.appendChild(dynmessageEl)

  const fnUpdateDynmessage = async (domain, message, refreshRateSeconds) => {
    let messageEdit = message
    if (domain) {
      const serverStatusResponse = await fetch(`https://${domain}/api/status`)
      const serverStatus = await serverStatusResponse.json()
      const serverConfigResponse = await fetch(`https://${domain}/api/config`)
      const serverConfig = await serverConfigResponse.json()

      messageEdit = messageEdit.replace(/\n/g, '<br>')
      messageEdit = messageEdit.replace(/{{viewerCount}}/g, serverStatus.viewerCount)
      messageEdit = messageEdit.replace(/{{viewerCount}}/g, serverStatus.viewerCount)
      messageEdit = messageEdit.replace(/{{overallMaxViewerCount}}/g, serverStatus.overallMaxViewerCount)
      messageEdit = messageEdit.replace(/{{sessionMaxViewerCount}}/g, serverStatus.sessionMaxViewerCount)
      messageEdit = messageEdit.replace(/{{versionNumber}}/g, serverStatus.versionNumber)
      messageEdit = messageEdit.replace(/{{name}}/g, serverConfig.name)
      messageEdit = messageEdit.replace(/{{title}}/g, serverConfig.title)
      messageEdit = messageEdit.replace(/{{summary}}/g, serverConfig.summary)
    }

    const dynmessageEl = document.querySelector('.dynmessage')
    dynmessageEl.innerHTML = messageEdit

    if (refreshRateSeconds > 0) {
      setTimeout(() => fnUpdateDynmessage(domain, message, refreshRateSeconds), refreshRateSeconds)
    }
  }

  fnUpdateDynmessage(domain, message, refreshRateSeconds)
}

const runUtility = () => {
  document.querySelector('.utility').addEventListener('submit', (evt) => {
    evt.preventDefault()
  })

  const updateURL = (evt) => {
    const messageEl = document.querySelector('.utility .message')
    const domainEl = document.querySelector('.utility .domain')
    const refreshEl = document.querySelector('.utility .refresh')
    const targetEl = document.querySelector('.utility .target')

    if (messageEl.value) {
      targetEl.innerHTML = `/dynmessage#${encodeURIComponent(messageEl.value)}/${encodeURIComponent(domainEl.value)}/${encodeURIComponent(refreshEl.value)}`
      targetEl.href = `/dynmessage#${encodeURIComponent(messageEl.value)}/${encodeURIComponent(domainEl.value)}/${encodeURIComponent(refreshEl.value)}`
    } else {
      targetEl.innerHTML = `/dynmessage`
      targetEl.href = `/dynmessage`
    }
  }

  document.querySelector('.utility .message').addEventListener('input', updateURL)
  document.querySelector('.utility .domain').addEventListener('input', updateURL)
  document.querySelector('.utility .refresh').addEventListener('input', updateURL)
  updateURL()
}

if (window.location.hash) {
  const el = document.body.querySelector('#instructions')
  el.parentNode.removeChild(el)
  runDynmessage(window.location.hash.substring(1))
} else {
  const el = document.body.querySelector('#tool')
  el.parentNode.removeChild(el)
  runUtility()
}
