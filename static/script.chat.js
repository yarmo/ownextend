const runPolling = (input) => {
  const params = input.split('&').reduce((res, item) => {
    var parts = item.split('=')
    res[parts[0]] = decodeURIComponent(parts[1])
    return res
  }, {})

  const pollingUrl = `https://${decodeURIComponent(params.domain)}/api/integrations/chat`
  const pollingInterval = 1000

  let lastRenderedChatId = null

  const fnRenderChat = (chat) => {
    const chatEl = document.querySelector('#chat_scroll')

    const chatRev = chat.reverse()
    let chatsToRender = []

    if (lastRenderedChatId) {
      let i = 0, searchingForLastRenderedChat = true
      while (searchingForLastRenderedChat) {
        if (chatRev[i].id == lastRenderedChatId) {
          searchingForLastRenderedChat = false
        } else {
          chatsToRender.push(chatRev[i])
        }

        if (i == chatRev.length-1) {
          searchingForLastRenderedChat = false
        }

        i++
      }
    } else {
      chatsToRender = chatRev
    }

    if (chatRev.length > 0) {
      lastRenderedChatId = chatRev[0].id
    }
    chatsToRender = chatsToRender.reverse()

    chatsToRender.forEach((item, i) => {
      if (item.type !== 'CHAT') {
        return
      }
      const nodeHTML = `
        <div class="message_author">
          <p style="color: hsl(${item.user.displayColor}, 45%, 60%)">${item.user.displayName}</p>
        </div>
        <div class="message_body">
          ${item.body}
        </div>
      `
      let newMessageEl = document.createElement('div')
      newMessageEl.classList.add('message')
      newMessageEl.innerHTML = nodeHTML
      chatEl.appendChild(newMessageEl)
    })

    chatEl.scrollTo(0, chatEl.scrollHeight)
  }

  const fnFetchChat = (end) => {
    fetch(pollingUrl, {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${params.token}`
      }
    })
    .then((result) => {
      return result.json()
    })
    .then((result) => {
      end(null, result)
    })
  }

  const polling = AsyncPolling(fnFetchChat, pollingInterval)

  polling.on('result', (result) => {
    fnRenderChat(result)
  })

  polling.run()
  // polling.stop()
}

const runUtility = () => {
  document.querySelector('.utility').addEventListener('submit', (evt) => {
    evt.preventDefault()
  })

  const updateURL = (evt) => {
    const domainEl = document.querySelector('.utility .domain')
    const tokenEl = document.querySelector('.utility .token')
    const targetEl = document.querySelector('.utility .target')

    if (domainEl.value && tokenEl.value) {
      targetEl.innerHTML = `/chat#domain=${encodeURIComponent(domainEl.value)}&token=${encodeURIComponent(tokenEl.value)}`
      targetEl.href = `/chat#domain=${encodeURIComponent(domainEl.value)}&token=${encodeURIComponent(tokenEl.value)}`
    } else {
      targetEl.innerHTML = `/chat`
      targetEl.href = `/chat`
    }
  }

  document.querySelector('.utility .domain').addEventListener('input', updateURL)
  updateURL()
}

if (window.location.hash) {
  const el = document.body.querySelector('#instructions')
  el.parentNode.removeChild(el)
  runPolling(window.location.hash.substring(1))
} else {
  const el = document.body.querySelector('#tool')
  el.parentNode.removeChild(el)
  runUtility()
}
