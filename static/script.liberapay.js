const runDynmessage = (input) => {
  const match = input.match(/([^\/]*)\/([^\/]*)(?:\/([^\/]))?/)
  const message = decodeURIComponent(match[1])
  const username = decodeURIComponent(match[2])
  const refreshRateSeconds = match[3] ? parseInt(decodeURIComponent(match[3])) : 0
  const toolEl = document.querySelector('#tool')

  let dynmessageEl = document.createElement('p')
  dynmessageEl.classList.add('dynmessage')
  toolEl.appendChild(dynmessageEl)

  const fnUpdateDynmessage = async (username, message, refreshRateSeconds) => {
    let messageEdit = message
    if (username) {
      const response = await fetch(`https://liberapay.com/${username}/public.json`)
      const data = await response.json()

      messageEdit = messageEdit.replace(/\n/g, '<br>')
      messageEdit = messageEdit.replace(/{{username}}/g, data.username)
      messageEdit = messageEdit.replace(/{{displayName}}/g, data.display_name)
      messageEdit = messageEdit.replace(/{{avatar}}/g, data.avatar)
      messageEdit = messageEdit.replace(/{{givingAmount}}/g, data.giving.amount)
      messageEdit = messageEdit.replace(/{{givingCurrency}}/g, data.giving.currency)
      messageEdit = messageEdit.replace(/{{receivingAmount}}/g, data.receiving.amount)
      messageEdit = messageEdit.replace(/{{receivingCurrency}}/g, data.receiving.currency)
      messageEdit = messageEdit.replace(/{{goal}}/g, data.goal)
      messageEdit = messageEdit.replace(/{{nPatrons}}/g, data.npatrons)
    }

    const dynmessageEl = document.querySelector('.dynmessage')
    dynmessageEl.innerHTML = messageEdit

    if (refreshRateSeconds > 0) {
      setTimeout(() => fnUpdateDynmessage(username, message, refreshRateSeconds), refreshRateSeconds)
    }
  }

  fnUpdateDynmessage(username, message, refreshRateSeconds)
}

const runUtility = () => {
  document.querySelector('.utility').addEventListener('submit', (evt) => {
    evt.preventDefault()
  })

  const updateURL = (evt) => {
    const messageEl = document.querySelector('.utility .message')
    const usernameEl = document.querySelector('.utility .username')
    const refreshEl = document.querySelector('.utility .refresh')
    const targetEl = document.querySelector('.utility .target')

    if (messageEl.value && usernameEl.value) {
      targetEl.innerHTML = `/liberapay#${encodeURIComponent(messageEl.value)}/${encodeURIComponent(usernameEl.value)}/${encodeURIComponent(refreshEl.value)}`
      targetEl.href = `/liberapay#${encodeURIComponent(messageEl.value)}/${encodeURIComponent(usernameEl.value)}/${encodeURIComponent(refreshEl.value)}`
    } else {
      targetEl.innerHTML = `/liberapay`
      targetEl.href = `/liberapay`
    }
  }

  document.querySelector('.utility .message').addEventListener('input', updateURL)
  document.querySelector('.utility .username').addEventListener('input', updateURL)
  document.querySelector('.utility .refresh').addEventListener('input', updateURL)
  updateURL()
}

if (window.location.hash) {
  const el = document.body.querySelector('#instructions')
  el.parentNode.removeChild(el)
  runDynmessage(window.location.hash.substring(1))
} else {
  const el = document.body.querySelector('#tool')
  el.parentNode.removeChild(el)
  runUtility()
}
