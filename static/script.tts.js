const runPolling = (domain) => {
  const pollingUrl = `https://${decodeURIComponent(domain)}/api/chat`
  const pollingInterval = 5000

  const dateStart = new Date()

  let lastScannedChatId = null
  let chatsToSpeak = []

  const fnScanChat = (chat) => {
    const chatRev = chat.reverse()
    let chatsToScan = []

    if (lastScannedChatId) {
      let i = 0, searchingForLastRenderedChat = true
      while (searchingForLastRenderedChat) {
        if (chatRev[i].id == lastScannedChatId) {
          searchingForLastRenderedChat = false
        } else {
          chatsToScan.push(chatRev[i])
        }

        if (i == chatRev.length-1) {
          searchingForLastRenderedChat = false
        }

        i++
      }
    } else {
      chatsToScan = chatRev
    }

    if (chatRev.length > 0) {
      lastScannedChatId = chatRev[0].id
    }
    chatsToScan = chatsToScan.reverse()

    chatsToScan.forEach((item, i) => {
      if (!item.visible || item.type !== 'CHAT') {
        return
      }
      if (dateStart.getTime() > new Date(item.timestamp)) {
        return
      }

      const bodyMatch = item.body.match(/\<p\>(.*)\<\/p\>/)

      if (!bodyMatch) {
        return
      }

      const message = bodyMatch[1]
      
      const reValidCommand = /\!speak (.*)/
      if (reValidCommand.test(message)) {
        item.parsedMessage = message.match(reValidCommand)[1]
        chatsToSpeak.push(item)
      }
    })

    fnSpeakChat()
  }

  const fnSpeakChat = () => {
    const chatEl = document.querySelector('#chat')

    if (responsiveVoice.isPlaying()) {
      return
    }

    const el = document.body.querySelector('.message')
    if (el) {
      el.parentNode.removeChild(el)
    }

    if (chatsToSpeak.length === 0) {
      return
    }

    // Unescaping the string to speak
    const txtEl = document.createElement("textarea")
    txtEl.innerHTML = chatsToSpeak[0].parsedMessage

    const color = uniqolor(chatsToSpeak[0].author, {
      saturation: [35, 70],
      lightness: [60, 80],
    })

    const nodeHTML = `
      <div class="message_author">
        <p style="color: ${color.color}">${chatsToSpeak[0].author} says:</p>
      </div>
      <br>
      <div class="message_body">
        ${chatsToSpeak[0].parsedMessage}
      </div>
    `

    let newMessageEl = document.createElement('p')
    newMessageEl.classList.add('message')
    newMessageEl.innerHTML = nodeHTML
    chatEl.appendChild(newMessageEl)

    // Send chat to TTS service
    responsiveVoice.speak(txtEl.value, 'UK English Female')

    // Remove chat from queue
    chatsToSpeak.splice(0,1)
  }

  const fnFetchChat = (end) => {
    fetch(pollingUrl)
    .then((result) => {
      return result.json()
    })
    .then((result) => {
      end(null, result)
    })
  }

  const polling = AsyncPolling(fnFetchChat, pollingInterval)

  polling.on('result', (result) => {
    fnScanChat(result)
  })

  polling.run()
  // polling.stop()
}

const runUtility = () => {
  document.querySelector('.utility').addEventListener('submit', (evt) => {
    evt.preventDefault()
  })

  const updateURL = (evt) => {
    const domainEl = document.querySelector('.utility .domain')
    const targetEl = document.querySelector('.utility .target')

    if (domainEl.value) {
      targetEl.innerHTML = `/tts#${encodeURIComponent(domainEl.value)}`
      targetEl.href = `/tts#${encodeURIComponent(domainEl.value)}`
    } else {
      targetEl.innerHTML = `/tts`
      targetEl.href = `/tts`
    }
  }

  document.querySelector('.utility .domain').addEventListener('input', updateURL)
  updateURL()
}

if (window.location.hash) {
  const el = document.body.querySelector('#instructions')
  el.parentNode.removeChild(el)
  runPolling(window.location.hash.substring(1))
} else {
  const el = document.body.querySelector('#tool')
  el.parentNode.removeChild(el)
  runUtility()
}
