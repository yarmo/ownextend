const runCountdown = (input) => {
  const match = input.match(/([0-9]*)(?:\/(.*))?/)
  const countdownSeconds = decodeURIComponent(match[1])
  const message = match[2] ? decodeURIComponent(match[2]) : `I'll be back in`
  dayjs.extend(window.dayjs_plugin_relativeTime)

  const toolEl = document.querySelector('#tool')
  const timeThen = dayjs().add(countdownSeconds, 'second')
  const timeNow = dayjs()

  let countdownEl = document.createElement('p')
  countdownEl.classList.add('countdown')
  toolEl.appendChild(countdownEl)

  const fnUpdateCountdown = (timeThen, message) => {
    const timeNow = dayjs()
    const countdownEl = document.querySelector('.countdown')
    countdownEl.innerHTML = `${message} ${timeNow.to(timeThen, 'true')}`

    if (timeNow < timeThen) {
      setTimeout(() => fnUpdateCountdown(timeThen, message), 1000)
    }
  }

  fnUpdateCountdown(timeThen, message)
}

const runUtility = () => {
  document.querySelector('.utility').addEventListener('submit', (evt) => {
    evt.preventDefault()
  })

  const updateURL = (evt) => {
    const timeEl = document.querySelector('.utility .time')
    const messageEl = document.querySelector('.utility .message')
    const targetEl = document.querySelector('.utility .target')

    if (timeEl.value) {
      if (messageEl.value) {
        targetEl.innerHTML = `/countdown#${encodeURIComponent(timeEl.value)}/${encodeURIComponent(messageEl.value)}`
        targetEl.href = `/countdown#${encodeURIComponent(timeEl.value)}/${encodeURIComponent(messageEl.value)}`
      } else {
        targetEl.innerHTML = `/countdown#${encodeURIComponent(timeEl.value)}`
        targetEl.href = `/countdown#${encodeURIComponent(timeEl.value)}`
      }
    } else {
      targetEl.innerHTML = `/countdown`
      targetEl.href = `/countdown`
    }
  }

  document.querySelector('.utility .time').addEventListener('input', updateURL)
  document.querySelector('.utility .message').addEventListener('input', updateURL)
  updateURL()
}

if (window.location.hash) {
  const el = document.body.querySelector('#instructions')
  el.parentNode.removeChild(el)
  runCountdown(window.location.hash.substring(1))
} else {
  const el = document.body.querySelector('#tool')
  el.parentNode.removeChild(el)
  runUtility()
}
